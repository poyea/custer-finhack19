import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button} from 'react-native';
import HomeScreen from './src/tabs/home';
import CommunityScreen from './src/tabs/community';
import InvestmentScreen from './src/tabs/investment';
import ProfileScreen from './src/tabs/profile';
import Blink from './src/tabs/info';
import SwiperScreen from './src/tabs/swipe';
import BondSelection from './src/page/BondSelection/BondSelection';
import CategorySelection from './src/page/CategorySelection/CategorySelection';
//import { TabNavigator } from 'react-navigation';
import {createStackNavigator, DrawerNavigator, createDrawerNavigator, createAppContainer} from 'react-navigation';


const MainNavigator = createStackNavigator(
  {
  Home: HomeScreen,
  Community: CommunityScreen,
  },
  {
    initialRouteName: "Home"
  }
);

const App = createDrawerNavigator({
  Home: {
    screen: HomeScreen,
  },
  Community: {
    screen: CommunityScreen,
  },
  Profile: {
    screen: ProfileScreen,
  },
  Investment: {
    screen: InvestmentScreen,
  },
  Intro: {
    screen: SwiperScreen,
  }
});

export default createAppContainer(App);

/*
const App = createAppContainer(MainNavigator);

export default App;
*/

/*
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
        <Text style={styles.instructions}>To get started, edit App.js</Text>
        <Text style={styles.instructions}>{instructions}</Text>
      </View>
    );
  }
}*/
// for further refactor
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
