import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button} from 'react-native';


class CommunityScreen extends React.Component {
    static navigationOptions = {
      title: 'Community',
    };
    render() {
      return (
        <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
          <Text>Community Screen</Text>
          <Button
            title="Go to Home"
            color="#000000"
            onPress={() => this.props.navigation.navigate('Home')}
          />
        </View>
      );
    }
}

export default CommunityScreen;