import React, { Component } from 'react'
import {StyleSheet, Text, View } from 'react-native'

import Swiper from 'react-native-swiper'

const styles = StyleSheet.create({
    wrapper: {},
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF'
    },
    slide2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF'
    },
    slide3: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF'
    },
    text: {
        color: '#000000',
        fontSize: 32,
        fontWeight: 'bold'
    }
})

class SwiperScreen extends Component {
    render() {
        return (
            <Swiper style={styles.wrapper} showsButtons={false}>
                <View style={styles.slide1}>
                    <Text style={styles.text}>"We {"\n"}foster {"\n"}<Text style={{ color: '#006400' }}>impact {"\n"}investing.</Text>" </Text>
                </View>
                <View style={styles.slide2}>
                    <Text style={styles.text}>"The {"\n"}<Text style={{ color: '#006400' }}>gamified {"\n"}experience {"\n"}</Text>is unprecedented." </Text>
                </View>
                <View style={styles.slide3}>
                    <Text style={styles.text}>"<Text style={{ color: '#006400' }}>Project</Text>-based {"\n"}investment" </Text>
                </View>
            </Swiper>
        )
    }
}

export default SwiperScreen;