import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button} from 'react-native';

class ProfileScreen extends React.Component {
    static navigationOptions = {
      title: 'Profile',
    };
    render() {
      return (
        <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
          <Text>Profile Screen</Text>
          <Button
            title="Go to Home"
            color="#000000"
            onPress={() => this.props.navigation.navigate('Home')}
          />
        </View>
      );
    }
}

export default ProfileScreen;
