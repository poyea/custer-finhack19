import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button} from 'react-native';


class InvestmentScreen extends React.Component {
    static navigationOptions = {
      title: 'Investment',
    };
    render() {
      return (
        <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
          <Text>Investment Screen</Text>
          <Button
            title="Go to Home"
            color="#000000"
            onPress={() => this.props.navigation.navigate('Home')}
          />
        </View>
      );
    }
}

export default InvestmentScreen;
