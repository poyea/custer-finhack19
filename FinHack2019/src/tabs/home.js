import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button} from 'react-native';


class HomeScreen extends React.Component {
    static navigationOptions = {
      title: 'Home',
      drawerIcon: () => (
        <Text>🏠</Text>
      ),
      headerStyle: {
        backgroundColor: '#f4511e',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },

    };
    render() {
      return (
        <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
          <Text>Home Screen</Text>
          <Button
            title="Go to Community"
            color="#000000"
            onPress={() => this.props.navigation.navigate('Community')}
          />
        </View>
      );
    }
}

export default HomeScreen;
