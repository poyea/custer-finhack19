import React, { Component } from 'react';
import { AppRegistry, ImageBackground, View, ScrollView, Button, FlatList, StyleSheet, Text } from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';

class VerticalCardList extends Component {
  render() {
    return (
    	<FlatList 
    	data={this.props.data}
    	renderItem={({item}) => 
    		<View style={styles.cardListItem}>
				<Text style={styles.cardHeader}> {item.name} </Text>
				<Text style={styles.Issuer}> {item.Issuer} </Text>
				<Text style={styles.CurrentAmount}> {"Amount Financed: " + item.CurrentAmount} </Text>
				<Text style={styles.TargetAmount}> {"Project Amount: " + item.TotalAmount} </Text>
				<Text style={styles.Type}> {"#" + item.Type} </Text>
				<View style={styles.ButtonWrapper}>
					<Button
					  title="Learn More"
					  color="#841584"
					/>
				</View>
			</View>
        }/>
    );
  }
}

class BondSelection extends React.Component {
	static navigationOptions = {
	  title: 'Bond Selection',
	};
  render() {
  	const { navigation } = this.props;
	const bondData = navigation.getParam('data', {});

    return (
      <ScrollView>
      	<VerticalCardList data={bondData}/>
       </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
	container: {
		marginLeft: 8,
		marginRight: 8,
	},
	header: {
		fontSize: 18,
		fontWeight: "bold",
		// padding: 16,
	},
	ButtonWrapper : {
		marginTop: 16,
		// fontSize: 18,
		// textAlign: "center",
	},
	cardHeader: {
		fontSize: 24,
		fontWeight: "bold",
		// padding: 16,
	},
	listItem : {
		padding: 16,
		fontSize: 18,
	    borderWidth: 0.5,
    	borderColor: '#d6d7da',
	},
	ButtonWrapper : {
		marginTop: 16,
		// fontSize: 18,
		// textAlign: "center",
	},
	cardListItem : {
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 2,
		},
		shadowOpacity: 0.25,
		shadowRadius: 3.84,
		elevation: 5,
		overflow: 'hidden',
		borderRadius: 4,
		backgroundColor: '#e2e2e2',
		padding: 16,
		margin: 16,
		marginBottom: 4,
		fontSize: 18,
	    borderWidth: 0.5,
    	borderColor: '#d6d7da',
	},
	content : {
		fontSize: 20,
		marginTop: 8,
	},
	Issuer : {
		fontSize: 20,
	},
	CurrentAmount : {
		fontSize: 20,
	},
	TargetAmount : {
		fontSize: 20,
	},
	Type : {
		fontSize: 20,
		marginTop: 8,
	}
});

const MainNavigator = createStackNavigator(
	{
		BondSelection: BondSelection,
	}
);

export default createAppContainer(MainNavigator);