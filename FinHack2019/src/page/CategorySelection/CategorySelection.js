import React, { Component } from 'react';
import { AppRegistry, ImageBackground, View, ScrollView, Button, FlatList, StyleSheet, Text } from 'react-native';
import button_img1 from './s1.jpg'
import button_img2 from './s2.jpg'
import button_img3 from './s3.jpg'
import button_img4 from './s4.jpg'
import button_img5 from './s5.jpg'
import { createStackNavigator, createAppContainer } from 'react-navigation';
import BondSelection from '../BondSelection/BondSelection';

class HorizontalButton extends Component {
  render() {
  	const imgList = {
  		Solar: button_img1,
		Wind: button_img2,
		Crop: button_img3,
		Fixtures: button_img4,
		Green: button_img5,
  	}

    return (
    	<FlatList 
    	style={{paddingBottom: 8, marginBottom: 8, marginTop: 16}}
    	horizontal={true}
    	alwaysBounceVertical={false}
    	data={this.props.data}
    	renderItem={({item}) => 
	    	<ImageBackground 
	    	source={imgList[item.key]}
	    	style={styles.buttonItem}
	    	>
				<Text style={styles.buttonText}>{item.name}</Text>
			</ImageBackground>
        }/>
    );
  }
}

class VerticalList extends Component {
  render() {
    return (
    	<FlatList 
    	data={this.props.data}
    	renderItem={({item}) => 
			<Text 
			style={styles.listItem}
			onPress={() => this.props.navigation.navigate('BondSelection', { data: bondData[item.key] })}>
				{item.name}
			</Text>
        }/>
    );
  }
}

class CategorySelection extends Component {
	static navigationOptions = {
	  title: 'Category Selection',
	};
  render() {
    return (
      <ScrollView>
  		<Text style={styles.header}>Hot Category</Text>
    	<HorizontalButton data={catData}/>
      	<Text style={styles.header}>View All Category</Text>
      	<VerticalList navigation={this.props.navigation} data={catData}/>
       </ScrollView>
    );
  }
}

const catData = [
		{
			key: 'Solar',
			name: "Solar Energy"
		}, 
		{
			key: 'Wind',
			name: "Wind Energy"
		}, 
		{
			key: 'Crop',
			name: "Crop Rotation"
		}, 
		{
			key: 'Fixtures',
			name: "Efficient Water Fixtures"
		}, 
		{
			key: 'Green',
			name: "Green Space"
		}
	];

const bondData = {
	Solar:[
		{
			key: 'a',
			name: "Air Cooled Chiller Replacement",
			Issuer: "MTR",
			CurrentAmount: "HK$ 32 millions",
			TotalAmount: "HK$ 11 billions",
			Type: "Energy Efficiency"

		}, 
		{
			key: 'b',
			name: "Trackside Energy Storage (pilot)",
			Issuer: "MTR",
			CurrentAmount: "HK$ 20 millions",
			TotalAmount: "HK$ 20 millions",
			Type: "Energy Efficiency"

		}, 
		{
			key: 'c',
			name: "South Island Line",
			Issuer: "MTR",
			CurrentAmount: "HK$ 17.2 millions",
			TotalAmount: "HK$ 5.56 billions",
			Type: "Low Carbon Transport"
		}, 
	],
	Wind:[
		{
			key: 'a',
			name: "Air Cooled Chiller Replacement",
			Issuer: "MTR",
			CurrentAmount: "HK$ 32 millions",
			TotalAmount: "HK$ 11 billions",
			Type: "Energy Efficiency"

		}, 
		{
			key: 'b',
			name: "Trackside Energy Storage (pilot)",
			Issuer: "MTR",
			CurrentAmount: "HK$ 20 millions",
			TotalAmount: "HK$ 20 millions",
			Type: "Energy Efficiency"

		}, 
		{
			key: 'c',
			name: "South Island Line",
			Issuer: "MTR",
			CurrentAmount: "HK$ 17.2 millions",
			TotalAmount: "HK$ 5.56 billions",
			Type: "Low Carbon Transport"
		}, 
	],
	Crop:[
		{
			key: 'a',
			name: "Air Cooled Chiller Replacement",
			Issuer: "MTR",
			CurrentAmount: "HK$ 32 millions",
			TotalAmount: "HK$ 11 billions",
			Type: "Energy Efficiency"

		}, 
		{
			key: 'b',
			name: "Trackside Energy Storage (pilot)",
			Issuer: "MTR",
			CurrentAmount: "HK$ 20 millions",
			TotalAmount: "HK$ 20 millions",
			Type: "Energy Efficiency"

		}, 
		{
			key: 'c',
			name: "South Island Line",
			Issuer: "MTR",
			CurrentAmount: "HK$ 17.2 millions",
			TotalAmount: "HK$ 5.56 billions",
			Type: "Low Carbon Transport"
		}, 
	],
	Fixtures:[
		{
			key: 'a',
			name: "Air Cooled Chiller Replacement",
			Issuer: "MTR",
			CurrentAmount: "HK$ 32 millions",
			TotalAmount: "HK$ 11 billions",
			Type: "Energy Efficiency"

		}, 
		{
			key: 'b',
			name: "Trackside Energy Storage (pilot)",
			Issuer: "MTR",
			CurrentAmount: "HK$ 20 millions",
			TotalAmount: "HK$ 20 millions",
			Type: "Energy Efficiency"

		}, 
		{
			key: 'c',
			name: "South Island Line",
			Issuer: "MTR",
			CurrentAmount: "HK$ 17.2 millions",
			TotalAmount: "HK$ 5.56 billions",
			Type: "Low Carbon Transport"
		}, 
	],
	Green:[
		{
			key: 'a',
			name: "Air Cooled Chiller Replacement",
			Issuer: "MTR",
			CurrentAmount: "HK$ 32 millions",
			TotalAmount: "HK$ 11 billions",
			Type: "Energy Efficiency"

		}, 
		{
			key: 'b',
			name: "Trackside Energy Storage (pilot)",
			Issuer: "MTR",
			CurrentAmount: "HK$ 20 millions",
			TotalAmount: "HK$ 20 millions",
			Type: "Energy Efficiency"

		}, 
		{
			key: 'c',
			name: "South Island Line",
			Issuer: "MTR",
			CurrentAmount: "HK$ 17.2 millions",
			TotalAmount: "HK$ 5.56 billions",
			Type: "Low Carbon Transport"
		}, 
	],
};

const styles = StyleSheet.create({
	container: {
		marginLeft: 8,
		marginRight: 8,
	},
	button: {
		padding: "16",
		marginRight: "16",
	},
	header: {
		fontSize: 24,
		backgroundColor: '#e2e2e2',
		fontWeight: "700",
		padding: 16,
	},
	buttonItem : {
		justifyContent: 'center', 
		alignItems: 'center',
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 2,
		},
		shadowOpacity: 0.25,
		shadowRadius: 3.84,
		elevation: 5,
		overflow: 'hidden',
		borderRadius: 4,
		width: 200, 
		height: 100, 
		marginLeft: 8,
		marginRight: 8
	},
	buttonText : {
		padding: 8,
		fontSize: 18,
		textAlign: "center",
		backgroundColor: '#e2e2e285',
	},
	listItem : {
		padding: 16,
		fontSize: 18,
	    borderWidth: 0.5,
    	borderColor: '#d6d7da',
	}
});

const MainNavigator = createStackNavigator(
	{
		CategorySelection: CategorySelection,
		BondSelection: BondSelection,
	}
);

export default createAppContainer(MainNavigator);